# Terms Of Service

## Usage Agreement
By inviting Emboard and using its functionalities (commands, website), you agreeing to the below mentioned Terms of Service.

You acknowledge that you have the right to use the bot freely on any Discord Server (Server) you share with it, that you can invite it to any Server that you have "Manage Server" rights for and that this privilege might get revoked for you, if you are subject of breaking the terms and/or policy of this bot, or the [Terms of Service](https://discord.com/terms), [Privacy Policy](https://discord.com/privacy) and/or [Community Guidelines](https://discord.com/guidelines) of [Discord Inc](https://discord.com).

Through Inviting the bot may it collect specific data as described in its [Privacy Policy](https://gitlab.com/evolvedmesh/emboard-static/-/blob/main/PRIVACY_POLICY.md).
The intended usage of this data is for core functionalities of the bot such as command handling, server-specific configuration.

## Intended Age
The bot may not be used by individuals under the minimal age described in [Discord's Terms of Service](https://discord.com/terms).

## Affiliation
The Bot is not affiliated with, supported or made by Discord Inc.  
Any direct connection to Discord or any of its Trademark objects is purely coincidental. We do not claim to have the copyright ownership of any of Discord's assets, trademarks or other intellectual property.

## Liability

The owner of the bot may not be made liable for individuals breaking these Terms nor Discord's [Terms of Service](https://discord.com/terms) at any given time.

We reserve the right to update these terms at our own discretion, giving you a 1-Week (7 days) period to opt out of these terms if you're not agreeing with the new changes.  
Any upcoming change to this Terms of Service will be notified to you through Emboard's support server.
You may opt out by Removing the bot from any Server you have the rights for.

## Contact

People may get in contact through the official [Support Server](https://discord.gg/mpZY2Pz) for Emboard.  
Other ways of support may be provided but aren't guaranteed.

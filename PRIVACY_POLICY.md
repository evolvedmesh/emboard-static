# Privacy Policy
## Usage of Data
The bot may use stored data, as defined below, for different features including but not limited to: Leaderboard system, Warnings system, Server config for all features that require storing data.
No usage of data outside of the aforementioned cases will happen and the data is not shared with any 3rd-party site or service.

### Stored Information
- Server IDs. These are stored to make server specific features possible.
- User IDs. These are stored for the purpose of making the leaderboard system possible (adding points, removing points etc.).
- Interaction IDs (or message IDs). These are used as identifier for a warning (Part of the warnings system).
- User Emails. These are stored upon logging into Emboard's [Website](https://emboard.evolvedmesh.com/dashboard).
- User Avatar. These are stored upon logging into Emboard's [Website](https://emboard.evolvedmesh.com/dashboard).
- User Guilds (Basic information (Partial Guild data type according to discord API documentation) of the servers the user is currently in). These are stored upon logging into Emboard's [Website](https://emboard.evolvedmesh.com/dashboard).

No other information outside of the above mentioned bullets will be stored.

## Updating Data

The data may be updated when using specific commands and performing actions in Emboard's [Website](https://emboard.evolvedmesh.com/dashboard).

No other actions may update the stored information at any given time.

## Removal of Data
Removal of specific data and all data related to a specific server can be removed from the 'Danger Zone Options' action in Emboard's [Website](https://emboard.evolvedmesh.com/dashboard).
